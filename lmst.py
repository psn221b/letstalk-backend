from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import wordnet , stopwords
from nltk.wsd import lesk
import nltk
from spelling_check import *

lemmatizer = WordNetLemmatizer()
stemmer = PorterStemmer()

# dictonary to exclude items and stop_words to be excluded
# stop_words = set(stopwords.words("english"))

exclude = {
    'the'       : True,
    'is'        : True,
    'are'       : True,
    'am'        : True,
    'have'      : True,
    'had'       : True,
    'has'       : True,
    'been'      : True,
    'be'        : True,
    'were'      : True,
    'being'     : True,
    'an'        : True,
    # 'and'       : True,
    'as'        : True,
    "'s"         : True,
    't'         : True,
    'd'         : True,
    "'ll"        : True,
    'm'         : True,
    'o'         : True,
    're'        : True,
    've'        : True,
    'y'         : True,
    'ain'       : True,
    'aren'      : True,

}

excptn = {
    # 'beautiful' : 'beauty',
    'his'       : 'his',
    'quarentine': 'quarentine',
    'only'      : 'only',
    # 'plenty'    : 'plenty',
    'having'    : 'have',
    'n\'t'      : 'not',
    'not'       : 'not',
    'ca'        : 'can',
    'thi'       : 'this'
    
    # word_tokenize will break these words example: could + n't
    # "aren't"    : 'not',
    # "couldn't"  : "could not",
    # "didn't"    : "did not",
    # "hasn't"    : "has not",
    # "hadn't"    : "had not",
    # "haven't"   : "have not",
    # "isn't"     : "not",
    # "didn't"    : "did not",
    # "doesn't"   : "does not",
    # "mightn't"  : "might not",
    # "mustn't"   : "must not",
    # "needn't"   : "needn't",
    # "shouldn't" : "should not",
    # "wasn't"    : "was not",
    # "weren't"   : "were not",
    # "won't"     : "will not",
    # "wouldn't"  : "would not",
}


# function to convert nltk tag to wordnet tag
def nltk_tag_to_wordnet_tag(nltk_tag):
    if nltk_tag.startswith('J'):
        return wordnet.ADJ
    elif nltk_tag.startswith('V'):
        return wordnet.VERB
    elif nltk_tag.startswith('N'):
        return wordnet.NOUN
    # elif nltk_tag.startswith('P'):
    #     return wordnet.NOUN
    elif nltk_tag.startswith('R'):
        return wordnet.ADV
    else:
        return None

def lemmatize_word(word, pos):
    if pos== 'NNP' or pos == 'NNPS':
        return word

    tag = nltk_tag_to_wordnet_tag(pos)
    word = word.lower()
    if word in excptn:
        return excptn[word]

    # removing adverb from the final sentense
    if (tag == wordnet.ADV):
        return
    # print(word)
    if word in exclude:# or word in stop_words:
        return
    
    word = stemmer.stem(word)
    if tag is None:

        # if there is no available tag then take the word as noun
        return correct_spell(lemmatizer.lemmatize(word))
    else:
        # else use the tag to lemmatize the token
        # if tag == wordnet.ADJ:
        #     return lemmatizer.lemmatize(word, tag)
        return correct_spell(lemmatizer.lemmatize(word, tag))
        


def resolve_context(word,tokens,pos):
    return lesk(tokens,word,nltk_tag_to_wordnet_tag(pos)).name().replace('.','%')

