from spellchecker import SpellChecker
#from sys import argv

spell = SpellChecker()

# find those words that may be misspelled
# misspelled = spell.unknown(['let', 'us', 'wlak','on','the','groun','beauti'])

# Get a list of `likely` options
# print(spell.candidates(word))
# print(misspelled)

# sentence should be a list
def correct_spell(word):
    #dummy wordlist for the input word
    # print(word)
    word_l = []
    word_l.append(word)

    #Checking for the mispelled word in the list which contains only 1 word
    misspelled = spell.unknown(word_l)

    #Misspelled is a set
    #If the length of the set is 0, the word is spelled correctly
    if len(misspelled) != 0:
        
        return spell.correction(misspelled.pop())

    #If we do not need a replacement
    return word

if __name__ == "__main__":
    # taking a sentence as input and spliting it with space
    z = argv[1].split()
    # Looping over the words in the sentence argument in commandline
    for w in z:
        # print(w)
        print(correct_spell(w))