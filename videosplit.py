from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import sys
import os
from moviepy.editor import VideoFileClip


# cuts=sys.argv
# print(cuts)
# x=sys.argv[1].split(",")
# print(x)

def video_split(marker, filename, upload_dir):
    final_file_dir = os.path.join(upload_dir, filename)
    clip = VideoFileClip(final_file_dir)
    time = marker.split(",")
    time = [i for i in time if i]
    file_list = []
    if len(time) == 0:
        ffmpeg_extract_subclip(final_file_dir, float(0), float(clip.duration),
                               targetname=os.path.join(upload_dir, "cut_" + str(0) + "_" + filename))
        file_list.append(os.path.join(upload_dir, "cut_" + str(0) + "_" + filename))
        return file_list
    ffmpeg_extract_subclip(final_file_dir, float(0), float(time[0]) / 1000,
                           targetname=os.path.join(upload_dir, "cut_" + str(0) + "_" + filename))
    file_list.append(os.path.join(upload_dir, "cut_" + str(0) + "_" + filename))
    for i in range(len(time) - 1):
        ffmpeg_extract_subclip(os.path.join(upload_dir, filename), float(time[i]) / 1000, float(time[i + 1]) / 1000,
                               targetname=os.path.join(upload_dir, "cut_" + str(i + 1) + "_" + filename))
        file_list.append(os.path.join(upload_dir, "cut_" + str(i + 1) + "_" + filename))
    ffmpeg_extract_subclip(final_file_dir, float(time[len(time) - 1]) / 1000,
                           float(clip.duration),
                           targetname=os.path.join(upload_dir, "cut_" + str(len(time)) + "_" + filename))
    file_list.append(os.path.join(upload_dir, "cut_" + str(len(time)) + "_" + filename))
    clip.close()
    return file_list
