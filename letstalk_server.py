import atexit
import os
import re
import threading
from concurrent.futures import thread
from copy import deepcopy
from queue import Queue
from signal import signal, SIGINT

from flask import Flask, flash, request, redirect, url_for, json, Response
from werkzeug.utils import secure_filename
from flask_sqlalchemy import SQLAlchemy
from settings import *
from lmst import *
from videosplit import *

UPLOAD_FOLDER = 'temp_videos'
ALLOWED_EXTENSIONS = {'mp4', 'avi'}
app = Flask(__name__)

# app.config.from_object(os.environ['APP_SETTINGS'])
app.config.from_object(os.getenv("APP_SETTINGS"))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

dead_files = Queue()
END_OF_DATA = object()  # a unique sentinel value


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/letstalk/getISL/')
def get_isl_urls():
    from word_to_reference import get_references_from_words

    sentence = request.args.get('sentence')
    urls_arr = get_references_from_words(sentence.lower())
    return Response(json.dumps(urls_arr), mimetype='application/json')


@app.route('/letstalk/getText/', methods=['POST'])
def get_text():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            user_id = filename[filename.rfind('_') + 1: filename.rfind('.')]
            upload_dir = os.path.join(app.config['UPLOAD_FOLDER'], user_id)
            Path(upload_dir).mkdir(parents=True, exist_ok=True)
            file.save(os.path.join(upload_dir, filename))
            markers = request.args.get('markers')
            file_list = video_split(markers, filename, upload_dir)  # final segmented videos in order
            # DO ML PROCESSING HERE

            # Delete temp files asynchronously
            dead_files.put(os.path.join(upload_dir, filename))
            for cut_file in file_list:
                dead_files.put(cut_file)
        return_txt = {  # Return sentence in this dict. Replace "HELLO WORLD" with final string
            "text": "HELLO WORLD"
        }
        return json.dumps(return_txt)


# dead files consumer
def background_deleter():
    while True:
        path = dead_files.get()
        if path is END_OF_DATA:
            return
        try:
            print('deleted {}'.format(path))
            os.remove(path)
        except:
            print('Error')
            pass


# close deleter thread
def close_running_threads():
    dead_files.put(END_OF_DATA)
    deleter.join()


# Handle Ctrl-C forced shutdown
def sigint_handler(sig, frame):
    close_running_threads()
    sys.exit(0)


if __name__ == "__main__":
    # Uncomment below 4 lines before deploying
    # deleter = threading.Thread(target=background_deleter)
    # deleter.start()
    # atexit.register(close_running_threads)  # register callback at shutdown of server
    # signal(SIGINT, sigint_handler)  # register same callback when server forced to shutdown using Ctrl-C
    app.run(host='0.0.0.0')
